import 'dart:math';

class CalculatorBrain {
  CalculatorBrain({this.weight, this.height});

  int height;
  int weight;
  double _bmi;

  String calculateBMI() {
    _bmi = weight / pow(height / 100, 2);
    return _bmi.toStringAsFixed(1);
  }

  String getResult() {
    if (_bmi >= 40) {
      return "Morbit Obezite";
    } else if (_bmi >= 35) {
      return "İkinci Dereceden Obezite";
    } else if (_bmi >= 30) {
      return "Birinci Dereceden Obezite";
    } else if (_bmi >= 25.1) {
      return "Balık Etli";
    } else if (_bmi >= 20) {
      return "İdeal";
    } else {
      return "Zayıf";
    }
  }

  String getInterpretation() {
    if (_bmi >= 40) {
      return "Çok tehlikeli bir durumdasınız, çok çok acil kilo vermelisiniz";
    } else if (_bmi >= 35) {
      return "Çookkk şişmansınız, çok acil kilo vermelisiniz";
    } else if (_bmi >= 30) {
      return "Kilonuz olması gerekeden çok fazla, kilo vermelisiniz";
    } else if (_bmi >= 25.1) {
      return "İdeal kilonuza çok yaklaşmışsınız, biraz daha gayret";
    } else if (_bmi >= 20) {
      return "Tebrikleerrr. Olmanız gereken kilodasınız, böyle devam";
    } else {
      return "Çok zayıfsınız, daha çok yemelisiniz";
    }
  }
}
